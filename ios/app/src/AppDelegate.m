/*
 * Copyright @ 2018-present 8x8, Inc.
 * Copyright @ 2017-2018 Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "AppDelegate.h"
#import "FIRUtilities.h"
#import "Types.h"
#import "UIAlertView+Params.h"
@import Crashlytics;
@import Fabric;
@import Firebase;
@import JitsiMeet;


@implementation AppDelegate

-             (BOOL)application:(UIApplication *)application
  didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Initialize Crashlytics and Firebase if a valid GoogleService-Info.plist file was provided.
    if ([FIRUtilities appContainsRealServiceInfoPlist]) {
        NSLog(@"Enablign Crashlytics and Firebase");
        [FIRApp configure];
        [Fabric with:@[[Crashlytics class]]];
    }

    JitsiMeet *jitsiMeet = [JitsiMeet sharedInstance];

    jitsiMeet.conferenceActivityType = JitsiMeetConferenceActivityType;
    jitsiMeet.customUrlScheme = @"org.jitsi.meeting";

    jitsiMeet.universalLinkDomains = @[@"meeting.hoptructuyen.xyz", @"alpha.jitsi.net", @"beta.meet.jit.si"];

    jitsiMeet.defaultConferenceOptions = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {

      builder.serverURL = [NSURL URLWithString:@"https://meeting.dttt.vn/"];
        builder.welcomePageEnabled = YES;

        // Apple rejected our app because they claim requiring a
        // Dropbox account for recording is not acceptable.
#if DEBUG
        [builder setFeatureFlag:@"ios.recording.enabled" withBoolean:YES];
#endif
    }];

    [jitsiMeet application:application didFinishLaunchingWithOptions:launchOptions];
//  [self checkUpdate];
    return YES;
}

-(void)setOption{
  JitsiMeetConferenceOptions *conferenceOptions = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {
      builder.serverURL = [NSURL URLWithString:@"https://meeting.dttt.vn/"];
      builder.room = self.roomID;
      builder.userInfo.displayName = self.userName;
      builder.audioOnly = NO;
      builder.videoMuted = NO;
      builder.audioMuted = NO;
      builder.welcomePageEnabled = NO;
  }];
//  [JitsiMeet sharedInstance]
//  return [[JitsiMeet sharedInstance] setPropsInViews:[conferenceOptions asProps]];
}

#pragma mark Linking delegate methods

-    (BOOL)application:(UIApplication *)application
  continueUserActivity:(NSUserActivity *)userActivity
    restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> *restorableObjects))restorationHandler {

    if ([FIRUtilities appContainsRealServiceInfoPlist]) {
        // 1. Attempt to handle Universal Links through Firebase in order to support
        //    its Dynamic Links (which we utilize for the purposes of deferred deep
        //    linking).
        BOOL handled
          = [[FIRDynamicLinks dynamicLinks]
                handleUniversalLink:userActivity.webpageURL
                         completion:^(FIRDynamicLink * _Nullable dynamicLink, NSError * _Nullable error) {
           NSURL *firebaseUrl = [FIRUtilities extractURL:dynamicLink];
           if (firebaseUrl != nil) {
             userActivity.webpageURL = firebaseUrl;
             [[JitsiMeet sharedInstance] application:application
                                continueUserActivity:userActivity
                                  restorationHandler:restorationHandler];
           }
        }];

        if (handled) {
          return handled;
        }
    }

    // 2. Default to plain old, non-Firebase-assisted Universal Links.
    return [[JitsiMeet sharedInstance] application:application
                              continueUserActivity:userActivity
                                restorationHandler:restorationHandler];
}
-(void)checkUpdate
{
  
  NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
  
  // Setup the request with URL
  NSURL *url = [NSURL URLWithString:@"https://liveclass.dttt.vn/version_hop?type=2"];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
  
  // Convert POST string parameters to data using UTF8 Encoding
  NSString *postParams = @"";
  NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
  
  // Convert POST string parameters to data using UTF8 Encoding
  [urlRequest setHTTPMethod:@"GET"];
  //    [urlRequest setHTTPBody:postData];
  
  // Create dataTask
  NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    //JSON Parsing....
    NSInteger status = [[NSString stringWithFormat:@"%@", results[@"status"]] integerValue];
    
    if (status == 1) {
      NSString* requiredVersion = [NSString stringWithFormat:@"%@", results[@"data"][@"name"]];
      NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
      if (![requiredVersion isEqualToString:version]) {
        // actualVersion is lower than the requiredVersion
        
        if ([results[@"data"][@"linkDownload"] length] > 0) {
          self.linkUpdate = results[@"data"][@"linkDownload"];
          //[self showAlertConfirmUpdate];
        } else {
//          return;
        }
      }else{
//        return;
      }
      
    }else{
      
      
//      return;
    }
    
    
  }];
  
  // Fire the request
  [dataTask resume];
}

// Force update to continue using
- (void)showAlertConfirmUpdate {
    if (self.alert != nil) {
        [self.alert dismissWithClickedButtonIndex:0 animated:YES];
    }
    self.alert = [[UIAlertView alloc] initWithTitle:nil message:@"Hệ thống yêu cầu cập nhật phiên bản mới để có thể sử dụng" delegate:self cancelButtonTitle:@"Cập nhật" otherButtonTitles:nil];
    
    [self.alert setParam:self.linkUpdate];
    [self.alert setTag:1003];
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
      NSLog(@"Do some work");
      
//      [self.alert show];
    });
  
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            NSString *linkUpdate = alertView.param;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkUpdate]];
            exit(0);
        } break;
        case 1:
            break;
    }
}

-(void)getToken
{
  NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
  
  NSString * strUrl =[NSString stringWithFormat:@"http://14.225.3.18:7777/token_request.php?user=%@&displayName=%@&admin=false",self.userName,self.userName];
  
  // Setup the request with URL
  NSURL *url = [NSURL URLWithString:strUrl];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
  
  [urlRequest setHTTPMethod:@"GET"];
  //    [urlRequest setHTTPBody:postData];
  
  // Create dataTask
  NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    NSString *token = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *strURL  = [NSString stringWithFormat:@"%@=%@",[self.url absoluteString],token];
    self.url = [NSURL URLWithString:strURL];
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
      NSLog(@"Do some work");
      
      if (self.roomID.length > 0) {
        [self checkUpdate];
        [[JitsiMeet sharedInstance] application:self.app
                                        openURL:self.url
                                        options:self.options];
      }
    });
    
  }];
  
  // Fire the request
  [dataTask resume];
}

- (BOOL)application:(UIApplication *)app
openURL:(NSURL *)url
options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {

    // This shows up during a reload in development, skip it.
    // https://github.com/firebase/firebase-ios-sdk/issues/233
    if ([[url absoluteString] containsString:@"google/link/?dismiss=1&is_weak_match=1"]) {
        return NO;
    }

    NSURL *openUrl = url;

    if ([FIRUtilities appContainsRealServiceInfoPlist]) {
        // Process Firebase Dynamic Links
        FIRDynamicLink *dynamicLink = [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
        NSURL *firebaseUrl = [FIRUtilities extractURL:dynamicLink];
        if (firebaseUrl != nil) {
            openUrl = firebaseUrl;
        }
    }

  NSArray *arr = [url.absoluteString componentsSeparatedByString:@"="];
  
  if (arr.count == 4) {
      self.roomID = arr[1];
      self.userName = arr[2];
    self.urlScheme = arr[3];
  }
  
  self.app = app;
  self.options = options;
  
  self.url = openUrl;
  
  if (self.isHas) {
//    return [[JitsiMeet sharedInstance] application:app
//                                               openURL:openUrl
//                                               options:options];
    
    [self getToken];
    return YES;
  }
  else{
    [self getToken];
    return YES;
    
  }
}

@end
