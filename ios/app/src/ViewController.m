/*
 * Copyright @ 2018-present 8x8, Inc.
 * Copyright @ 2017-2018 Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import <Availability.h>

@import CoreSpotlight;
@import MobileCoreServices;
@import Intents;  // Needed for NSUserActivity suggestedInvocationPhrase

@import JitsiMeet;

#import "Types.h"
#import "ViewController.h"
#import "AppDelegate.h"

@implementation ViewController
{
  AppDelegate *appDelegate;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
  JitsiMeetView *view = (JitsiMeetView *) self.view;
  view.delegate = self;
  self->appDelegate.isHas = YES;
  JitsiMeetConferenceOptions *options = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {
    
  }];
  
  [view join:options];
  [self checkUpdate];
  
  double delayInSeconds = 2.0;
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    NSLog(@"Do some work");
    
//      [self getToken];
  });
  
  
}

-(void)getToken
{
  NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
  
  NSString * strUrl =[NSString stringWithFormat:@"http://14.225.3.18:7777/token_request.php?user=%@&displayName=%@&admin=false",self->appDelegate.userName,self->appDelegate.userName];
  
  // Setup the request with URL
  NSURL *url = [NSURL URLWithString:strUrl];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
  
  [urlRequest setHTTPMethod:@"GET"];
  //    [urlRequest setHTTPBody:postData];
  
  // Create dataTask
  NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    NSString *token = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *strURL  = [NSString stringWithFormat:@"%@=%@",[self->appDelegate.url absoluteString],token];
    self->appDelegate.url = [NSURL URLWithString:strURL];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
      NSLog(@"Do some work");
      
      if (self->appDelegate.roomID.length > 0) {
        [self checkUpdate];
        [[JitsiMeet sharedInstance] application:self->appDelegate.app
                                        openURL:self->appDelegate.url
                                        options:self->appDelegate.options];
      }
    });
    
  }];
  
  // Fire the request
  [dataTask resume];
}

-(void)checkUpdate
{
  
  NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
  
  // Setup the request with URL
  NSURL *url = [NSURL URLWithString:@"https://liveclass.dttt.vn/version_hop?type=2"];
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
  
  // Convert POST string parameters to data using UTF8 Encoding
  NSString *postParams = @"";
  NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
  
  // Convert POST string parameters to data using UTF8 Encoding
  [urlRequest setHTTPMethod:@"GET"];
  //    [urlRequest setHTTPBody:postData];
  
  // Create dataTask
  NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    //JSON Parsing....
    NSInteger status = [[NSString stringWithFormat:@"%@", results[@"status"]] integerValue];
    
    if (status == 1) {
      NSString* requiredVersion = [NSString stringWithFormat:@"%@", results[@"data"][@"name"]];
      NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
      if (![requiredVersion isEqualToString:version]) {
        // actualVersion is lower than the requiredVersion
        
        if ([results[@"data"][@"linkDownload"] length] > 0) {
          self->appDelegate.linkUpdate = results[@"data"][@"linkDownload"];
          [self->appDelegate showAlertConfirmUpdate];
        } else {
//          return;
        }
      }else{
//        return;
      }
      
    }else{
      
      
//      return;
    }
    
    
  }];
  
  // Fire the request
  [dataTask resume];
}



//-(void) setOp:(JitsiMeetView *)view{
//  JitsiMeetConferenceOptions *conferenceOptions = [JitsiMeetConferenceOptions fromBuilder:^(JitsiMeetConferenceOptionsBuilder *builder) {
//      builder.serverURL = [NSURL URLWithString:@"https://live.hoptructuyen.xyz"];
//      builder.room = self->appDelegate.roomID;
//      builder.userInfo.displayName = self->appDelegate.userName;
//      builder.audioOnly = NO;
//      builder.videoMuted = NO;
//      builder.audioMuted = NO;
//      builder.welcomePageEnabled = NO;
//  }];
//
// // [view setPropsInViews:[conferenceOptions asProps]];
//}

// JitsiMeetViewDelegate


- (void)_onJitsiMeetViewDelegateEvent:(NSString *)name
                             withData:(NSDictionary *)data {
  NSLog(
        @"[%s:%d] JitsiMeetViewDelegate %@ %@",
        __FILE__, __LINE__, name, data);
  
#if DEBUG
  NSAssert(
           [NSThread isMainThread],
           @"JitsiMeetViewDelegate %@ method invoked on a non-main thread",
           name);
#endif
}

- (void)conferenceJoined:(NSDictionary *)data {
  [self _onJitsiMeetViewDelegateEvent:@"CONFERENCE_JOINED" withData:data];
  
  // Register a NSUserActivity for this conference so it can be invoked as a
  // Siri shortcut. This is only supported in iOS >= 12.
#ifdef __IPHONE_12_0
  if (@available(iOS 12.0, *)) {
    NSUserActivity *userActivity
    = [[NSUserActivity alloc] initWithActivityType:JitsiMeetConferenceActivityType];
    
    NSString *urlStr = data[@"url"];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSString *conference = [url.pathComponents lastObject];
    
    userActivity.title = [NSString stringWithFormat:@"Join %@", conference];
    userActivity.suggestedInvocationPhrase = @"Join my Online meeting";
    userActivity.userInfo = @{@"url": urlStr};
    [userActivity setEligibleForSearch:YES];
    [userActivity setEligibleForPrediction:YES];
    [userActivity setPersistentIdentifier:urlStr];
    
    // Subtitle
    CSSearchableItemAttributeSet *attributes
    = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:(NSString *)kUTTypeItem];
    attributes.contentDescription = urlStr;
    userActivity.contentAttributeSet = attributes;
    
    self.userActivity = userActivity;
    [userActivity becomeCurrent];
  }
#endif
  
}

- (void)conferenceTerminated:(NSDictionary *)data {
  [self _onJitsiMeetViewDelegateEvent:@"CONFERENCE_TERMINATED" withData:data];
//  if (self->appDelegate.roomID.length > 0) {
//    if (self->appDelegate.urlScheme.length > 0) {
//      NSString *callAppURL = [NSString stringWithFormat:@"%@://",self->appDelegate.urlScheme];
//      
//      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callAppURL]];
//    }
//    exit(0);
//  }
}

- (void)conferenceWillJoin:(NSDictionary *)data {
  [self _onJitsiMeetViewDelegateEvent:@"CONFERENCE_WILL_JOIN" withData:data];
  [self checkUpdate];
}

#if 0
- (void)enterPictureInPicture:(NSDictionary *)data {
  [self _onJitsiMeetViewDelegateEvent:@"ENTER_PICTURE_IN_PICTURE" withData:data];
}
#endif

@end
