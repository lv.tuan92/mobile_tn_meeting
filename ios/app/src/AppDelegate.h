/*
 * Copyright @ 2017-present Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, assign) BOOL isHas;
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) NSString *roomID;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) UIApplication *app;
@property (nonatomic, strong) NSString *urlScheme;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSDictionary<UIApplicationOpenURLOptionsKey,id> *options;
@property (nonatomic, strong) UIAlertView *alert;
@property (nonatomic, strong) NSString *linkUpdate;
- (void)showAlertConfirmUpdate;
@end
