//
//  UIAlertView.m
//  mobilelearning
//
//  Created by HungLe on 1/6/16.
//  Copyright © 2016 trinam. All rights reserved.
//

#import "UIAlertView+Params.h"
#import <objc/runtime.h>

@implementation UIAlertView (Params)

static void * paramPropertyKey = &paramPropertyKey;

- (NSString *)param {
    return objc_getAssociatedObject(self, paramPropertyKey);
}

- (void)setParam:(NSString *)param {
    objc_setAssociatedObject(self, paramPropertyKey, param, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}




@end
