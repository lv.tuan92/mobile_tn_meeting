//
//  UIAlertView.h
//  mobilelearning
//
//  Created by HungLe on 1/6/16.
//  Copyright © 2016 trinam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Params)
@property (nonatomic, copy) NSString* param; 
@end
