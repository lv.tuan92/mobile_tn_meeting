package trinam.com.meeting;

import androidx.appcompat.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;


import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import trinam.com.meeting.sdk.JitsiMeetActivity;
import trinam.com.meeting.sdk.JitsiMeetActivityDelegate;
import trinam.com.meeting.sdk.JitsiMeetConferenceOptions;
import trinam.com.meeting.sdk.JitsiMeetUserInfo;
import trinam.com.meeting.sdk.JitsiMeetView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class VirtualClassActivity extends JitsiMeetActivity {
    private JitsiMeetView view;

    @Override
    public void onActivityResult(
        int requestCode,
        int resultCode,
        Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        JitsiMeetActivityDelegate.onActivityResult(
            this, requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        JitsiMeetActivityDelegate.onBackPressed();
    }

    @Override
    public void onConferenceTerminated(Map<String, Object> data) {
        Log.d(TAG, "Conference terminated: " + data);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String strCode = "";
        String userName = "";
        String avatar = "";
        if (getIntent() != null) {
            strCode = getIntent().getStringExtra("KEY_ROONAME");
            userName = getIntent().getStringExtra("KEY_USERNAME");
        }

        if (strCode != null && !strCode.equals("")) {
            getToken(strCode, userName, userName, avatar);
        } else {
            setContentView(R.layout.activity_virtual_class);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("");
            builder.setMessage("Bạn phải đăng nhập từ app Elearning");
            builder.setPositiveButton("ĐÓNG",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            final AlertDialog dialog = builder.create();
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface arg) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                }
            });
            dialog.show();
        }
    }

    private void getToken(String strCode, String userName, String displayName, String avatar) {
        String url = "http://14.225.3.18:7777/token_request.php?user=" + userName + "&displayName=" + displayName + "&admin=false&avatar=" + avatar;
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception ex, String result) {
                    view = new JitsiMeetView(VirtualClassActivity.this);
                    JitsiMeetConferenceOptions options = null;
                    JitsiMeetUserInfo userInfo = new JitsiMeetUserInfo();
                    userInfo.setDisplayName(userName);//userName
                    try {
                        options = new JitsiMeetConferenceOptions.Builder()
                            .setServerURL(new URL("https://meeting.dttt.vn/"))
                            .setRoom(strCode)
                            .setUserInfo(userInfo)
                            .setWelcomePageEnabled(false)
                            .setFeatureFlag("call-integration.enabled", true)
                            .setToken(result)
                            .build();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    view.join(options);
                    setContentView(view);

                }
            });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        view.dispose();
        view = null;

        JitsiMeetActivityDelegate.onHostDestroy(this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        JitsiMeetActivityDelegate.onNewIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(
        final int requestCode,
        final String[] permissions,
        final int[] grantResults) {
        JitsiMeetActivityDelegate.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();

        JitsiMeetActivityDelegate.onHostResume(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        JitsiMeetActivityDelegate.onHostPause(this);
    }
}
