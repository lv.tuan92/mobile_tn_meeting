package trinam.com.meeting.common.response;

/**
 * Created by KIEN on 1/4/2016.
 */
public class Response {
    String success;
    String TimeServer;
    String session;
    String message;
    String status;

    public String getSuccess() {
        return success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String isSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getTimeServer() {
        return TimeServer;
    }

    public void setTimeServer(String timeServer) {
        this.TimeServer = timeServer;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String msg){
        this.message = msg;
    }
}

