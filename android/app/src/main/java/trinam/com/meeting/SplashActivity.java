package trinam.com.meeting;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import trinam.com.meeting.common.response.VersionResponse;
import trinam.com.meeting.common.utils.Constants;
import trinam.com.meeting.common.utils.Utils;
import trinam.com.meeting.common.utils.VNCharacterUtils;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashActivity extends Activity {
    // Progress Dialog
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        checkDevicePermission();
    }

    public void checkDevicePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> lstPermissions = new ArrayList<>();

            if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.CAMERA);
            }
            if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.RECORD_AUDIO);
            }

            if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAPTURE_AUDIO_OUTPUT)
                != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.CAPTURE_AUDIO_OUTPUT);
            }

            if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS)
                != PackageManager.PERMISSION_GRANTED) {
                lstPermissions.add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
            }

            if (lstPermissions.size() > 0) {
                String[] permissions = new String[lstPermissions.size()];
                for (int i = 0; i < lstPermissions.size(); i++) {
                    permissions[i] = lstPermissions.get(i);
                }
                ActivityCompat.requestPermissions(this, permissions, 10000);
            }else{
                checkNewVersion();
            }
        }else{
            checkNewVersion();
        }
    }

    private void checkNewVersion() {
        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(this, R.string.err_no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            File rootFile = new File(Environment.getExternalStorageDirectory().toString()
                + "/online_meet.apk");
            Utils.deleteRecursive(rootFile);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Ion.with(this)
            .load("https://liveclass.dttt.vn/version?type=1")
            .as(VersionResponse.class)
            .setCallback(new FutureCallback<VersionResponse>() {
                @Override
                public void onCompleted(Exception e, VersionResponse result) {
                    if (result != null && result.getData() != null) {
                        String version = "";
                        try {
                            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            version = pInfo.versionName;
                        } catch (PackageManager.NameNotFoundException exx) {
                            exx.printStackTrace();
                        }

                        String strLastVersion = result.getData().getName();
                        final String urlDownload = result.getData().getLinkDownload();
                        Utils.saveSharedPrefrece(Constants.CURRENT_VERSION_NAME, strLastVersion, SplashActivity.this);
                        Utils.saveSharedPrefrece(Constants.URL_NEW_VERSION, urlDownload, SplashActivity.this);
                        if (!version.equals(strLastVersion)) {
                            alertUpdateVersion(urlDownload);
                        } else {
//                            gotoPluginVersion();
                            gotoOnlineMeetVersion();
                        }
                    } else {
//                        gotoPluginVersion();
                        gotoOnlineMeetVersion();
                    }
                }
            });
    }

    private void gotoPluginVersion() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoVirtualActivity();
            }
        }, 2000);
    }

    private void gotoOnlineMeetVersion() {
        String strName = Utils.getSharedPrefrece("NAME", this);
        if (!strName.equals("")) {
            strName = VNCharacterUtils.removeAccent(strName);
            strName = strName.replaceAll(" ","");
            getToken(strName, strName, "");
        } else {
            gotoLoginActivity();
        }
    }

    private void getToken(String userName, String displayName, String avatar) {
        String url = "http://14.225.3.18:7777/token_request.php?user=" + userName + "&displayName=" + displayName + "&admin=false&avatar=" + avatar;
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception ex, String result) {
                    gotoMainActivity(userName, result);
                }
            });
    }

    private void alertUpdateVersion(final String urlDownload) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("");
        builder.setMessage("Có phiên bản mới, vui lòng cập nhật để tiếp tục sử dụng.");
        builder.setPositiveButton(getResources().getString(R.string.btn_confirm),
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new UpdateVersionTask().execute(urlDownload);
                }
            });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }
        });
        dialog.show();
    }

    private void gotoVirtualActivity() {
        String strCode = "";
        String userName = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strCode = bundle.getString("KEY_ROONAME");
            userName = bundle.getString("KEY_USERNAME");
        }
        Intent intent = new Intent(SplashActivity.this, trinam.com.meeting.VirtualClassActivity.class);
        intent.putExtra("KEY_USERNAME", userName);
        intent.putExtra("KEY_ROONAME", strCode);
        startActivity(intent);
        finish();
    }

    private void gotoMainActivity(String userName, String token) {
        Intent intent = new Intent(SplashActivity.this, trinam.com.meeting.MainActivity.class);
        intent.putExtra("NAME", userName);
        intent.putExtra("TOKEN", token);
        startActivity(intent);
        finish();
    }

    private void gotoLoginActivity() {
        Intent intent = new Intent(SplashActivity.this, trinam.com.meeting.LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10000:
                checkNewVersion();
        }
    }


    /**
     * Showing Dialog
     */

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Đang tải file...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    /**
     * Background Async Task to download file
     */
    class UpdateVersionTask extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                    .getExternalStorageDirectory().toString()
                    + "/online_meet.apk");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

        }

    }

}
