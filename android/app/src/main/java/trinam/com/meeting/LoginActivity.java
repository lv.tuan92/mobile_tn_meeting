package trinam.com.meeting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import trinam.com.meeting.common.utils.Utils;
import trinam.com.meeting.common.utils.VNCharacterUtils;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class LoginActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        EditText edName = (EditText) findViewById(R.id.edName);
        Button btnContinue = (Button) findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strName = edName.getText().toString().trim();
                if(!strName.equals("")){
                    strName = VNCharacterUtils.removeAccent(strName);
                    strName = strName.replaceAll(" ","");
                    Utils.saveSharedPrefrece("NAME", strName, LoginActivity.this);
                    getToken(strName, strName, "");
                }else{
                    Toast.makeText(LoginActivity.this, "Bạn phải nhập tên để tiếp tục", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getToken(String userName, String displayName, String avatar) {
        String url = "http://14.225.3.18:7777/token_request.php?user=" + userName + "&displayName=" + displayName + "&admin=false&avatar=" + avatar;
        Ion.with(this)
            .load(url)
            .asString()
            .setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception ex, String result) {
                    gotoMainActivity(userName, result);
                }
            });
    }

    private void gotoMainActivity(String userName, String token) {
        Intent intent = new Intent(LoginActivity.this, trinam.com.meeting.MainActivity.class);
        intent.putExtra("NAME", userName);
        intent.putExtra("TOKEN", token);
        startActivity(intent);
        finish();
    }
}
