package trinam.com.meeting.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;


/**
 * Created by KIEN on 1/21/2016.
 */
public class Utils {

    public static String encrypt(String data) {
        try {
            byte[] privateKey = Constants.PRIVATE_KEY.getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(privateKey, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(data.getBytes("UTF-8"));
            String result = Base64.encodeToString(encrypted, Base64.NO_WRAP);
            return result;
        } catch (Exception e) {
            return "";
        }
    }

    public static int getTimeDiff(String timeServer) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            String strCurDateMobile = df.format(new Date());

            Date d1 = df.parse(strCurDateMobile);
            Date d2 = df.parse(timeServer);
            long diff = d2.getTime() - d1.getTime();
            return (int) (diff / 1000);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String generateNewKey(String session, long timeDiff) {
        String newKey = session;// + FORMAT_KEY + formatDate(timeDiff);
        return encrypt(newKey);
    }

    public static Boolean isNetworkAvailable(Context context) {
        if (context == null) return true;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Xoa du lieu sharedprefrences
     */
    public static void clearSharedPreferencesData(Activity activity) {
        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor edit = shre.edit();
        String isFirstLaunch = getSharedPrefrece(Constants.FIRST_RUN, activity);
        String isAutoLogin = getSharedPrefrece(Constants.AUTO_LOGIN, activity);
        String theme = getSharedPrefrece(Constants.THEME, activity);

        String username = getSharedPrefrece(Constants.USERNAME, activity);
        String password = getSharedPrefrece(Constants.PASSWORD, activity);
        String rememberpassword = getSharedPrefrece(Constants.REMEMBER_PASSWORD, activity);

        edit.clear();
        edit.putString(Constants.FIRST_RUN, isFirstLaunch);
        edit.putString(Constants.AUTO_LOGIN, isAutoLogin);
        edit.putString(Constants.THEME, theme);

        edit.putString(Constants.USERNAME, username);
        edit.putString(Constants.PASSWORD, password);
        edit.putString(Constants.REMEMBER_PASSWORD, rememberpassword);

        edit.commit();
    }


    public static void saveSharedPrefrece(String key, String srtContent, Activity activity) {
        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor edit = shre.edit();
        edit.putString(key, srtContent);
        edit.commit();
    }

    public static String getSharedPrefrece(String key, Activity activity) {
        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(activity);
        String strContent = shre.getString(key, "");
        return strContent;
    }

    public static void saveSharedPrefreceInt(String key, int intContent, Activity activity) {
        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor edit = shre.edit();
        edit.putInt(key, intContent);
        edit.commit();
    }

    public static int getSharedPrefreceInt(String key, Activity activity) {
        SharedPreferences shre = PreferenceManager.getDefaultSharedPreferences(activity);
        int intContent = shre.getInt(key, 0);
        return intContent;
    }

    //========Encrypt/Decrypt========//
    public static void encrypt(String oldFile, String newFile) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        // Here you read the cleartext.
        FileInputStream fis = new FileInputStream(oldFile);
        // This stream write the encrypted text. This stream will be wrapped by another stream.
        FileOutputStream fos = new FileOutputStream(newFile);

        // Length is 16 byte
        // Careful when taking user input!!! https://stackoverflow.com/a/3452620/1188357
        SecretKeySpec sks = new SecretKeySpec(Constants.KEY_FILE.getBytes(), "AES");
        // Create cipher
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, sks);
        // Wrap the output stream
        CipherOutputStream cos = new CipherOutputStream(fos, cipher);
        // Write bytes
        int b;
        byte[] d = new byte[8];
        while ((b = fis.read(d)) != -1) {
            cos.write(d, 0, b);
        }
        // Flush and close streams.
        cos.flush();
        cos.close();
        fis.close();
        //Xoa file cu sau khi da ma hoa thanh cong
        File file = new File(oldFile);
        file.delete();
    }

    public static void decrypt(String oldFile, String newFile) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        FileInputStream fis = new FileInputStream(oldFile);
        FileOutputStream fos = new FileOutputStream(newFile);
        SecretKeySpec sks = new SecretKeySpec(Constants.KEY_FILE.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[8];
        while ((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
    }

    public static void removeFile(File rooteFile, String key) {
        File[] files = rooteFile.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isDirectory()) {
                    removeFile(file, key);
                } else {
                    if (!key.equals("")) {
                        if (file.getName().contains(key)) {
                            file.delete();
                        }
                    } else {
                        file.delete();
                    }

                }
            }
        }
    }

    public static void deleteRecursive(File fileOrDirectory) {
        try {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles())
                    deleteRecursive(child);

            fileOrDirectory.delete();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getFileExt(String fileName) {
        String result = "";
        try {
            result = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
            if (!result.substring(0, 1).equals(".")) {
                result = "." + result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static String getFileName(String fileName) {
        String result = "";
        try {
            if (!fileName.contains(".")) {
                result = fileName;
            } else {
                result = fileName.substring(0, fileName.lastIndexOf("."));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }


    public static long getFreeSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } else {
            bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        }
        long megAvailable = bytesAvailable / (1024 * 1024);
        Log.e("", "Available MB : " + megAvailable);
        return megAvailable;
    }

}
