package trinam.com.meeting.common.response;


import trinam.com.meeting.common.object.VersionInfo;

public class VersionResponse extends Response {
    private VersionInfo data = null;

    public VersionInfo getData() {
        return data;
    }

    public void setData(VersionInfo data) {
        this.data = data;
    }
}
