package trinam.com.meeting.common.utils;

public class Constants {
    public static final String DBNAME = "elearning.sqlite";
    public static final String DBLOCATION = "/data/data/com.viettel.elearning/databases/";

    public static String TYPE_MY_CLASS = "TYPE_MY_CLASS";
    public static String TYPE_SEARCH = "TYPE_SEARCH";
    public static String TYPE_MORE = "TYPE_MORE";
    public static String TYPE_HOME = "TYPE_HOME";
    public static String TYPE_MY_EXAMS = "TYPE_MY_EXAMS";

    public static String NAV_USERINFO = "NAV_USERINFO";
    public static String NAV_SETTING = "NAV_SETTING";
    public static String NAV_INFO = "NAV_INFO";
    public static String NAV_LOGOUT = "NAV_LOGOUT";

    public static String BROADCAST_SEARCH_MY_CLASS = "BROADCAST_SEARCH_MY_CLASS";
    public static String KEYWORD_SEARCH_MY_CLASS = "KEYWORD_SEARCH_MY_CLASS";
    public static String BROADCAST_KEYWORD = "BROADCAST_KEYWORD";
    public static String BROADCAST_SEARCH = "BROADCAST_SEARCH";
    public static String KEYWORD_SEARCH = "KEYWORD_SEARCH";

    public static String GOTO_MYCLASS = "GOTO_MYCLASS";
    public static String UPDATE_NOTIFICATION = "UPDATE_NOTIFICATION";
    public static String UPDATE_CONNECTION = "UPDATE_CONNECTION";

    public static String DELETE_NOTIFICATION = "DELETE_NOTIFICATION";
    public static String REGISTER_CLASS_STATE = "REGISTER_CLASS_STATE";
    public static String APPROVED_AFTER_REGISTER = "APPROVED_AFTER_REGISTER";


    public static final String FIRST_RUN = "firstRun";//lan dau cai app
    public static final String PRIVATE_KEY = "dj(*f-foau43[=0A";
    public static final String AUTO_LOGIN = "1";//1- Tự động đăng nhập
    public static final String THEME = "THEME";//cau hinh theme ung dung

    public static final String TOKEN_FIREBASE = "";

    public static String KEY_FILE = "";

    public static int EVENT_VIDEO_CALL = 1; //event gui request thuc hien video call
    public static int EVENT_SELECT_FILE = 2; //event truyen file
    public static int EVENT_START_SHARE_SCREEN = 3; //event share man hinh
    public static int EVENT_START_BOAR = 4; //event mo bang trang
    public static int EVENT_START_C_BOAR = 5; //event mo bang trang dung chung


    public static String MODE_SHARESCEEN = "SHARE SCREEN";
    public static String MODE_WHITEBOARD = "WHITE BOARD";
    public static String MODE_CAMERA = "CAMERA";
    public static String ROOM_TYPE_SCREEN = "screen";
    public static String ROOM_TYPE_BOARD = "board";
    public static String INTENT_FINTER_NEW_CLIENT_SCREEN = "INTENT_FINTER_NEW_CLIENT_SCREEN";

    public static int DOWNLOADING = 1;
    public static int DOWNLOADED = 2;

    public static final int EVENT_NOTIFICATION = 0;

    public static String REMEMBER_PASSWORD = "REMEMBER_PASSWORD";
    public static String USERID = "USERID";
    public static String USERNAME = "USERNAME";
    public static String PASSWORD = "PASSWORD";
    public static String CURRENT_VERSION_NAME = "CURRENT_VERSION_NAME";
    public static String URL_NEW_VERSION = "URL_NEW_VERSION";
    public static boolean serviceBound = false;
    public static boolean isRunningAudioActivity = false;
    public static final String UPDATE_PROGRESS_MUSIC = "UPDATE_PROGRESS_MUSIC";
    public static final String UPDATE_PROGRESS_MUSIC_ON_UI = "UPDATE_PROGRESS_MUSIC_ON_UI";
    public static final String NEXT_PREVIOUS_AUDIO = "NEXT_PREVIOUS_AUDIO";
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.valdioveliu.valdio.audioplayer.PlayNewAudio";


    public static String VIRTUALCLASS_CHUADIENRA = "1";
    public static String VIRTUALCLASS_DANGDIENRA = "2";
    public static String VIRTUALCLASS_KETTHUC = "3";

    public static String PATH_NAME = "Elearning";


    public static String START_RECEIVED_SLIDESHOW = "START_RECEIVED_SLIDESHOW";
    public static String CHANGE_PAGE_RECEIVED_SLIDESHOW = "CHANGE_PAGE_RECEIVED_SLIDESHOW";
    public static String STOP_RECEIVED_SLIDESHOW = "STOP_RECEIVED_SLIDESHOW";
    public static String ALLOW_DOWNLOAD = "ALLOW_DOWNLOAD";
    public static String LIMIT_DOWNLOAD = "LIMIT_DOWNLOAD";
    public static String DOWNLOAD_FILE_OFFLINE = "DOWNLOAD_FILE_OFFLINE";
    public static String DOWNLOAD_FILE_OFFLINE_DONE = "DOWNLOAD_FILE_OFFLINE_DONE";

    public static String UPDATE_TEST_TOTALTIME = "UPDATE_TEST_TOTALTIME";
    public static String UPDATE_TEST_QUESTIONTIME = "UPDATE_TEST_QUESTIONTIME";
    public static String UPDATE_TEST_SURVEY = "UPDATE_TEST_SURVEY";

    public static String HINHTHUC_TUHOC = "1";
    public static String HINHTHUC_ONLINE = "2";
    public static String HINHTHUC_OFFLINE = "3";
    public static String HINHTHUC_KETHOP = "4";

    public static String HINHTHUC_DANGKY_TUDO = "1";
    public static String HINHTHUC_DANGKY_COKIEMDUYET = "2";
    public static String HINHTHUC_DANGKY_KHONGCHODANGKY = "3";


    //TYPE DOCUMENT
    public static String document_textHtml = "B196BFC0-591D-497D-9128-87E2746B9494";
    public static String document_document = "22F7F46E-ACC2-4259-899A-950C3CD69EFB";
    public static String document_image = "E22E50D7-2E02-4E78-88A7-E6C1E9B88A83";
    public static String document_zip = "4F659B1A-89CB-48B7-93EE-A91CA32DF720";
    public static String document_scorm = "37D237C9-3433-4709-AF38-3CC65718C429";
    public static String document_video = "C2A2E16A-A364-42B6-804E-66EB96905304";


    //TYPE CONTENT
    public static String content_TinCan = "1669e60c-3a96-4efc-b4ea-18411ec328ba";
    public static String content_SCORM = "37d237c9-3433-4709-af38-3cc65718c429";
    public static String content_Video = "c2a2e16a-a364-42b6-804e-66eb96905304";
    public static String content_thi_kiemtra = "b6421bc8-2324-4510-9217-68babde82313";
    public static String content_textHtml = "b196bfc0-591d-497d-9128-87e2746b9494";
    public static String content_link = "df07d9ba-e605-450d-87f3-8e474983f314";
    public static String content_document = "22f7f46e-acc2-4259-899a-950c3cd69efb";
    public static String content_zip = "4f659b1a-89cb-48b7-93ee-a91ca32df720";
    public static String content_khaosat = "7bd609d4-33bb-43e2-8c1d-c5bf008780bf";
    public static String content_hinhanh = "e22e50d7-2e02-4e78-88a7-e6c1e9b88a83";
    public static String content_offline = "42ee141a-f4dc-457e-a487-fc655fdc5506";
    public static String content_Ifame = "f18d633f-d0fa-4ce4-ad31-4a2490a5b70c";


    //TYPE KHAO SAT
    public static String khaosat_chon_mot = "1";
    public static String khaosat_chon_nhieu = "2";
    public static String khaosat_chon_mot_tu_luan="3";
    public static String khaosat_chon_nhieu_tu_luan="4";
    public static String khaosat_matran_chon_mot="5";
    public static String khaosat_matran_chon_nhieu="6";
    public static String khaosat_tu_luan="7";

    //Type THI
    public static String THI_ONE_CHOICE = "1";
    public static String THI_MULTI_CHOICE = "2";
    public static String THI_AS = "3";
    public static String THI_FILL_BLANK = "4";
    public static String THI_TRUE_FALSE = "5";
    public static String THI_YES_NO = "6";
    public static String THI_MATCHING = "7";
    public static String THI_PULLDOWN_MENU = "8";
    public static String THI_SUB_QUESTION = "9";


    //Type loai thi
    public static String BAITHI_EQ = "4";


    public static final int SECOND = 1000;
    public static String TEMPLATE_CLOCK_NORMAL = "%02d : %02d : %02d";
    public static String TEMPLATE_CLOCK_WARNING = "<font color='red'>%02d : %02d : %02d</font>";


    //=========================NOTIFICATION===========================//
    public static int NOTIFICATION_ID = 0;
    public static String Noti1 = "Noti1";//Email nhắc nhở làm bài kiểm tra	Noti1
    public static String Noti2 = "Noti2";//Email xác nhận đăng ký lớp học	Noti2
    public static String Noti3 = "Noti3";//Email thông báo lớp học được gán bởi quản trị viên	Noti3
    public static String Noti4 = "Noti4";//Email thông báo lớp học bị từ chối bởi người quản lý/quản trị đào tạo	Noti4
    public static String Noti5 = "Noti5";//Email nhắc nhở về lớp học đã đăng ký nhưng chưa hoàn thành	Noti5
    public static String Noti6 = "Noti6";//Email thông báo hoàn thành lớp học	Noti6
    public static String Noti7 = "Noti7";// Email nhắc nhở khi không tham gia lớp học	Noti7
    public static String Noti8 = "Noti8";//Email thông báo về việc thay đổi vai trò trên hệ thống	Noti8
    public static String Noti9 = "Noti9";// Email thông báo về mật khẩu mới tới email người dùng (sử dụng tài khoản local trên ứng dụng) khi người dùng sử dụng chức năng reset mật khẩu tự động trên hệ thống	Noti9
    public static String Noti10 = "Noti10";//Email xác nhận được gán vào lớp do quản trị	Noti10
    public static String Noti11 = "Noti11";//Email thông báo lớp học được duyệt bởi người quản lý/quản trị đào tạo	Noti11
    public static String Noti12 = "Noti12";
    public static String Noti13 = "Noti13";

    //Discuss
    public static int REFRESH_DISCUSS = 1000;
}
