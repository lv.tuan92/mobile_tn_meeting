/**
 * Automatically generated file. DO NOT MODIFY
 */
package trinam.com.meeting;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "trinam.com.meeting";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 7405330;
  public static final String VERSION_NAME = "1.0.0";
  // Fields from build type: debug
  public static final boolean GOOGLE_SERVICES_ENABLED = false;
  public static final boolean LIBRE_BUILD = false;
}
